<?php
namespace app\modules\news\controllers;

use yii\web\Controller;
use app\modules\news\models;

class DefaultController extends Controller {
    public function actionIndex() {
        $model = new models\News();

        return $this->render('index', ['model' => $model]);
    }

    public function actionView($path) {
        $model = new models\News();
        $article = $model->findArticleByPath($path);

        if(!is_null($article)) {
            return $this->render('view', ['model' => $article]);
        } else {
            return $this->render('404');
        }
    }
}