<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>
<div class='row'>
    <div class='col-sm-7'>
        <div class='alert alert-warning'>
            <div class='alert-heading'>
                <?= $model['title']; ?>
            </div>
            <p>
                <?php
                echo Html::a(
                    'Назад к списку новостей',
                    Url::to(['/news/'])
                )
                ?>
            </p>
        </div>
    </div>
</div>
