<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>

<div class='row'>
    <div class='col-sm-7'>
        <?php foreach($model->articles as $article): ?>
            <div class='alert alert-warning'>
                <div class='alert-heading'>
                    <?= $article['title']; ?>
                </div>
                <p>
                    <?php
                    echo Html::a(
                            'Читать далее',
                            Url::to(['/news/default/view/', 'path' => $article['path']])
                        )
                    ?>
                </p>
            </div>
        <?php endforeach; ?>
    </div>
</div>