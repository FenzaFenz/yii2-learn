<?php
namespace app\modules\news;

use \yii\base\Module;

class News extends Module {
    public $controllerNamespace = 'app\modules\news\controllers';
    public $layout = 'main';

    public function init() {
        parent::init();
    }
}