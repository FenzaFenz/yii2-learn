<?php
namespace app\modules\news\models;

use yii\base\Model;

class News extends Model {
    public $articles;

    public function init() {
        parent::init();
        $this->articles = [
            ["title" => "Главная новость", "path" => "1"],
            ["title" => "Первая новость", "path" => "news1"],
            ["title" => "Вторая новость", "path" => "news2"],
        ];
    }

    public function findArticleByPath($path) {
        foreach ($this->articles as $article) {
            if($article["path"] == $path) {
                return $article;
            }
        }

        return null;
    }
}