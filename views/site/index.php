<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Тестовое задание</h1>
        <p>
            <?php
            echo Html::a(
                'Перейти к списку новостей',
                Url::to(['/news/']),
                ['class' => 'btn btn-lg btn-success']
            )
            ?>
        </p>
    </div>
</div>
